import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from art.defences.trainer import Trainer,AdversarialTrainer,AdversarialTrainerMadryPGD

class Defence():
    def __init__(self,Classifier, x_train, y_train):
        self.Classifier = Classifier
        self.x_train = x_train
        self.y_train = y_train

    def Create_Defence(self,option,attack):
        self.attack=attack
        if(option=="BCT"):#Base Class Trainer

            print("Applying Base Class Trainer".center(35,">"),"\n")
            trainer = Trainer(self.Classifier)

        elif(option=="Advtr"): #AdversarialTrainer
            
            print("Applying Adversarial Trainer".center(36,">"),"\n")
            trainer = AdversarialTrainer(classifier=self.Classifier,attacks=self.attack)

        elif(option=="AdvPGD"): #AdversarialTrainerMadryPGD

            print("Applying AdversarialTrainerMadryPGD".center(43,">"),"\n")
            trainer = AdversarialTrainerMadryPGD(classifier=self.Classifier)
        
        trainer.fit(self.x_train, self.y_train, batch_size=16, nb_epochs=10)
      
        return trainer.get_classifier()
