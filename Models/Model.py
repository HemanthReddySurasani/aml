import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras.models import model_from_json
from keras import Sequential
from keras.layers import Activation,MaxPooling2D,Conv2D,Dropout,Flatten,Dense
from art.estimators.classification import KerasClassifier

class Model():
    def __init__(self, x_train, y_train):
        self.x_train = x_train
        self.y_train = y_train

    def Create_Model(self):
        # Model Building
        def add_layer(argument):
            # Input layer
            if(argument=="0"):
                Model.add(Conv2D(int(input("Batch_size :")), (int(input("Strides x:")),int(input("y:"))), padding=input("padding is :"),input_shape=(180, 180, 3))),
            # Convulation layer
            elif(argument=="1"):
                Model.add(Conv2D(int(input("Batch_size :")), (int(input("Strides x:")), int(input("y:"))))),
            # Activation layer
            elif(argument=="2"):
                Model.add(Activation(input("Enter actiavtion :"))),
            # Maxpooling layer
            elif(argument=="3"):
                Model.add(MaxPooling2D(pool_size=((int(input("Maxpooling_size x:")), int(input("y:")))))),
            # Dropout layer
            elif(argument=="4"):
                Model.add(Dropout(float(input("Dropout Value :")))),
            # Flatten layer
            elif(argument=="5"):
                Model.add(Flatten()),
            # Dense layer
            elif(argument=="6"):
                Model.add(Dense(int(input("Value for Dense layer :"))))
            
        cmd=input("Wanna upload ur own 'Neural-Network' Enter 'Upload'\n\t\t OR \nBuild it here Enter 'Build' \n\t\t OR \nPress anykey for default Model: ")

        if(cmd=='Build'):
            Model = Sequential()
            Option = ""
            data = ["0-Input layer", "1-Convulation layer", "2-Activation layer",
                    "3-Maxpooling layer", "4-Dropout layer", "5-Flatten layer", "6-Dense layer"]
            while(Option != "End"):
                print("Building new Model".center(26,"*"),"\n")
                print("Initially Model = 'Sequential()' is done add layers with required parameters\n",*data,sep="\n")
                Option = input("Choose from above list or Enter 'End' \n")
                if(Option in ["0","1","2","3","4","5","6"]):
                    add_layer(Option)
                else:
                    raise Exception("Value out Range....")

            # compiling the Model
            print("Compiling Model".center(23,"*"),"\n")
            Model.compile(loss=input("Enter loss function :"), optimizer=input("Optimizer :"), metrics=[input("Enter the Metrics :")])
            print("Training Model".center(22,"*"),"\n")
            Model.fit(self.x_train,self.y_train,batch_size=int(input("Traing Batch_Size :")),epochs=int(input("No of Epochs :")))
            print("...Building of Model is Completed Successfully...".center(59,"*"),"\n")

        elif(cmd=='Upload'):

            print("Loading the saved model from json".center(41,"*"),"\n")
            json_file = open(input("Enter the path to 'json' file :"), 'r')

            loaded_model_json = json_file.read()
            json_file.close()
            Model = model_from_json(loaded_model_json)
            
            print("Loading weights into new model".center(38,"*"),"\n")
            Model.load_weights(input("Enter the path to '.h5' file :"))
            print("Compiling Model".center(23,"*"),"\n")
            Model.compile(loss=input("Enter loss function :"), optimizer=input("Optimizer :"), metrics=[input("Enter the Metrics :")])

        else:
            # load json and create model
            print("...Model Structure...\n"
                  "model = Sequential()\n"
                  "model.add(Conv2D(32, (3, 3), padding=same, input_shape=x_tr.shape[1:]))\n"
                  "model.add(Activation(relu))\n"
                  "model.add(Conv2D(32, (3, 3)))\n"
                  "model.add(Activation(relu))\n"
                  "model.add(MaxPooling2D(pool_size=(2, 2)))\n"
                  "model.add(Dropout(0.2))\n"
                  "model.add(Conv2D(64, (3, 3), padding=same))\n"
                  "model.add(Activation(relu))\n"
                  "model.add(Conv2D(64, (3, 3)))\n"
                  "model.add(Activation(relu))\n"
                  "model.add(MaxPooling2D(pool_size=(2, 2)))\n"
                  "model.add(Dropout(0.2))\n"
                  "model.add(Flatten())\n"
                  "model.add(Dense(512))\n"
                  "model.add(Activation(relu))\n"
                  "model.add(Dropout(0.25))\n"
                  "model.add(Dense(2))\n"
                  "model.add(Activation(softmax)))")

            print("Loading the saved model from json".center(41,"*"),"\n")
            json_file = open('/home/mouli/Documents/Project_Work/Models/model.json', 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            Model = model_from_json(loaded_model_json)
            # load weights into new model
            Model.load_weights("/home/mouli/Documents/Project_Work/Models/cnn.h5")
            print("Compiling Model".center(23,"*"),"\n")
            Model.compile(loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["accuracy"])


        classifier = KerasClassifier(model=Model,clip_values=(0.0,255.0))

        return classifier